class Scales():
    def __init__(self, music_notes, major_scale_intervals, root_note):
        self.music_notes = music_notes
        self.major_scale_intervals = major_scale_intervals
        self.root_note = root_note

    def musical_notes(self):
        self.music_notes = ["a", "a#", "b", "c", "c#", "d", "d#", "e", "f", "f#", "g", "g#"]
        self.music_notes = self.music_notes.upper()
        """if music note starts on d, that's on the 5th index
        which means that the index will continue counting up until
        index 11 at which point we will continue counting indexes from a."""

        self.root_note = input("Choose root: ").upper()
        #?? if root_note matches a note in list music_notes ??
        
        while self.music_notes[self.root_note] < self.music_notes[11]:
            self.music_notes = True
        else:
            self.music_notes = False

class Major_Scale(Scales):
    def major_scale_notes(self):
        
        self.major_scale_interval = self.music_notes[self.root_note:2:5] + self.music_notes[5:2:-1]
        print(self.major_scale_interval)

        if self.music_notes == True:
            print(self.music_notes[::-1])
        else:
            self.music_notes == False
            print(self.music_notes[::1])      

    def __str__(self) -> str:
        return self.music_notes

class Harmonic_Minor_Scale(Scales):
    pass

class Pentatonic(Scales):
    pass
