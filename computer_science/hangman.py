# :TermExec cmd="python %
# Set a global variable indicating 5 tries
# make a list of words and assign it a Global variable
# randomly choose a word from the list
# replace the letters of the word with a '*'.
# input prompt to guess the word
# while true -- replace all instances of '*' with correctly guessed letter
#   if tries is greater than '0', 
#   check for winner, if false show updated prompt
# elif letter == false or tries is greater than '0'
#   show updated prompt
# elif letter == false and tries is equal to '0'
# Exit You lose!

import random
import sys

tries = 5
word_list = ['random', 'acquiesce', 'green', 'hog']

random_word = random.choice(word_list)

dotted_word = len(random_word) * '*'
print(dotted_word)
guess_letter = input('Guess the letter: ')
print(guess_letter)

while True:
    if tries > 0:
        guessed_letter = input('Enter a letter: ')
        for i in random_word:
            if guessed_letter is i:
                print(dotted_word.replace('*', 'i'))
                print('Correct')
            else:
                print('Try again!')
                tries -= 1
    elif tries == 0:
        print('you lose')
        sys.exit()


