class Vehicle:

    fuel = "combustible"

    def __init__(self, color, vehicle_type, mileage):
        self.color = color
        self.mileage = mileage
        self.vehicle_type = vehicle_type

    def __str__(self):
        return f"The {self.color} {self.vehicle_type} has gone {self.mileage} miles."

class Car(Vehicle):
    pass

class Motorcycle(Vehicle):
    pass

class Bicycle(Vehicle):
    fuel = "non-combustible"

car1 = Car("blue", "car", "20,000")
car2 = Car("red", "truck", "30,000")

print(car1)
print(car2)

motorcycle = Motorcycle("black", "motorcycle", "50,000")
print(motorcycle)

bicycle = Bicycle("aqua", "bicycle", "10,000")
print(bicycle, "and uses", Bicycle.fuel, "fuel.")
print(type(car1))
print(type(motorcycle))