class MusicScale():
    def __init__(self, index, notes, root, scale):
        self.index = index
        self.notes = notes
        self.root = root
        self.scale = scale
        
    def choose_fret(self):
        fret = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]
        return fret

    def choose_index(self):
        index = 0
        return index
    
    def choose_root(self):
        root = 'A'
        return root
    
    def choose_notes(self): 
        notes = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'] * 3
        return notes


class MusicScaleType(MusicScale):
    def __init__(self, chromatic, major, nat_har_min, pentatonic):
        self.chromatic = chromatic
        self.major = major
        self.nat_har_min = nat_har_min
        self.pentatonic = pentatonic

        def choose_chromatic_scale(self):
            chromatic_scale = [notes[(y + notes.index(root))%len(notes)] for y in [0,1,2,3,4,5,6,7,8,9,10,11]]
            return chromatic_scale

        def choose_major_scale(self):
            major_scale = [notes[(y + notes.index(root))%len(notes)] for y in [0,2,4,5,7,9,11]]
            return major_scale

        def choose_natural_harmonic_minor(self):
            natural_harmonic_minor_scale = [notes[(y + notes.index(root))%len(notes)] for y in [0,2,3,5,7,8,11]]
            return natural_harmonic_minor_scale

        def choose_pentatonic_scale(self):
            pentatonic_scale = [notes[(y + notes.index(root))%len(notes)] for y in [0,3,5,7,10]]
            return pentatonic_scale
            