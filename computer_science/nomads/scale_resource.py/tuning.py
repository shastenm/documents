class GuitarTuning():

    def __init__(self, standard, drop_d, dadgad):
        self.standard = standard
        self.drop_d = drop_d
        self.dadgad = dadgad

    def use_standard_tuning(self):
        standard = [
            notes.index('E'),
            notes.index('B'),
            notes.index('G'),
            notes.index('D'),
            notes.index('A'),
            notes.index('E'),
    ]
        return standard

    def use_drop_d(self):
        drop_d = [
            notes.index('D'),
            notes.index('B'),
            notes.index('G'),
            notes.index('D'),
            notes.index('A'),
            notes.index('D'),
    ]
        return drop_d

    def use_dadgad(self):
        dadgad = [
            notes.index('D'),
            notes.index('A'),
            notes.index('G'),
            notes.index('D'),
            notes.index('A'),
            notes.index('D'),
    ]
        return dadgad
