# If a runner runs 10 miles in 30 minutes and 30 seconds,
# What is their average speed in kilometers per hour?
# (Tip: 1 mile = 1.6 km)

get_min_per_mile = 30.5 / 10
get_miles_per_hour = 60 / get_min_per_mile
convert_mile_kilometer = get_miles_per_hour * 1.6
print(convert_mile_kilometer)

