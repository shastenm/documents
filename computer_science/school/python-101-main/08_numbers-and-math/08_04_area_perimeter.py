# Write code to display the area and perimeter
# of a rectangle that has a width of 2.4 and a height of 6.4.
def get_area(height, width):
    area = height * width
    print(area)
get_area(6.4, 2.4)

def get_perimeter(height, width):
    perimeter = 2 * (height + width)
    print(perimeter)
get_perimeter(6.4, 2.4)
