# Using a loop, sum all numbers from the `start` to the `stop` number.
# The sequence should consist only of integers from 1 to 100.
# The output of your calculation should look like this:
#
#      The sum is: 5050

#start = 1
#stop = 100
#
#for i in range(start, stop):
#    start += 1
#    consec = i + start
#    print(consec)
s = ""


n = 5

while n > 0:

    n -= 1

    if (n % 2) == 0:

        continue


    a = ['foo', 'bar', 'baz']

    while a:

        s += str(n) + a.pop(0)

        if len(a) < 2:

            break