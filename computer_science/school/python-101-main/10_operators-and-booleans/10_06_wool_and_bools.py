# Write a code snippet that gives a name to a `sheep`
# and uses a boolean value to define whether it has `wool` or not.
sheeps_name = input("Sheep's name? ").upper()
if sheeps_name == "MICHAEL":
    print(sheeps_name, "doesn't have wool.")
else:
    print(sheeps_name, "has wool.")