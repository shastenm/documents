# Use string indexing and string concatenation
# to write the sentence "we see trees" only by picking
# the necessary letters from the given string.

word = "tweezers "
phrase = word[1:3], word[7:2:-2], word[0] + word[6] + word[2:4] + word[7]
print(phrase)
