import random
# Here's your first task
# Re-create the guess-my-number game from the course.
# Type the whole code out instead of copy-pasting.
# Typing out code, even if you just copy it, trains your coding skills!
# Write your code below:
num = random.randint(1,10)
guess = None

while num != guess:
    guess = int(input('Guess a number: '))
    if guess == num:
        print('You win')
        break
    else:
        print('Try again!')