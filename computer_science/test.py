whole_notes = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']*3
def get_notes(key, intervals):
    
    root = whole_notes.index(key)
    octave = whole_notes[root:root + 12]
    return [octave[i] for i in intervals]

    
MODE = {
            'IONIAN': [0,2,4,5,7,9,11],
            'DORIAN': [0,2,3,5,7,9,10],
            'PHRYGIAN': [0,1,3,5,7,9,10],
            'MIXOLYDIAN': [0,2,4,5,7,9,10],
            'LYDIAN': [0,2,4,6,7,9,11,12],
            'AEOLIAN': [0,2,3,5,7,10,11],
            'PENTATONIC_MINOR': [0,3,5,8,10],
            'PENTATONIC_MAJOR': [0,2,4,5,7,9],
            'HARMONIC_MINOR': [0,2,3,5,7,8,11], 
            'NATURAL_MINOR': [0,2,3,5,7,8,10]
            }
strings = {i:0 for i in 'EADGB'}
for i in strings.keys():
    start = whole_notes.index(i)
    strings[i] = whole_notes[start:start + 20]  
print(strings.keys()) 
print(strings['E'])

def find_notes(my_scale):
    notes_strings = {i:0 for i in "EADGB"}
    # for every string 
    for key in strings.keys():
        # we create an empty list of indexes
        indexes = []
        for note in my_scale.notes:
            # append index where note of the scale is found in
            ind = strings[key].index(note)
            indexes.append(ind)
            # because there are 20 frets, there are duplicate notes in the string
            if ind <= 7:
                # we must also append these to indexes
                indexes.append(ind+12)
        notes_strings[key] = indexes
    return notes_strings

E_major = get_notes('E', MODE['IONIAN'])
positions = find_notes(E_major)
print(positions)




 




        



