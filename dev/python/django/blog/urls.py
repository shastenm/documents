
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('css/animate.css', views.css_renderer),
    path('css/bootstrap.css', views.css_renderer),
    path('css/font-awesome.min.css', views.css_renderer),
    path('css/main.css', views.css_renderer),
    path('css/main.min.css', views.css_renderer),
    path('css/owl.carousel.css', views.css_renderer),
    path('css/owl.theme.min.css', views.css_renderer),

    ]