from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'blog/index.html')

def css_renderer(request):
    return render(request, '.css', {}, content_type="text/css")
