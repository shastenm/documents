import PySimpleGUI as sg

layout = [  [ sg.Text('Convert Kilos To Pounds!') ],
            [ sg.Text('Enter In The Number Of Kilos:') ],
            [ sg.Input(size=(8,1), key='-KILOS-') ],
            [ sg.Button('Calculate') ] ]

window = sg.Window('Convert Kilo To Pounds', layout)

while True:
    event, values = window.read()    
    if event in (None, "Exit"):
        break
    for key in values:
        
        values[key] = float(values[key])
      

    if event == 'Calculate':
        total_kilos = values['-KILOS-'] 
        kilo = 2.204
        total = total_kilos * kilo
        sg.popup(f'{total}')
        window['-KILOS-'].update(total)



window.close()