import PySimpleGUI as sg

layout = [  [ sg.Checkbox('Celsius'), sg.Checkbox('Fahrenheit') ],
            [ sg.Text('Convert:') ],
            [ sg.Input(size=(8,1), key='-INTPUT-') ],
            [ sg.Button('Convert') ] ]

window = sg.Window('Convert Kilo To Pounds', layout)

while True:
    event, values = window.read()    
    if event in (None, "Exit"):
        break
    for key in values:
        
        values[key] = float(values[key])
    def get_celsius():  
        if event == 'Celsius':
            return (values['-INPUT-'] * 9/5) + 32
    c = get_celsius()

    def get_fahrenheit():
        if event == 'Fahrenheit':
            return (values['-INPUT-'] * 1.8) + 32
    f = get_fahrenheit()
    
    def get_conversion():
        if event == 'Convert':
            window['-INPUT-'].update(c, f)



window.close()
