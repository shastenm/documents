#Vim Cheatsheet#

##Basic Commands##
###Insert Mode###
		* i - enter insert mode
		* I - enter in insert mode (1st nonblank character)
  	* s - delete character under cursor and insert mode
		* S - delete line and insert at beginning of line
		* a - insert mode at after character
		* A - insert mode at the end of the line
		* o - insert mode on the next line
		* O - insert mode on the above line
		* C - delete from cursor to the end of the line
		* w - forward one word
		* W - forward one WORD
		* b - backward one word
		* B - backward one WORD
		* e - forward to the next end of word
		* E - forward to the next end of WORD
		* f - until character 
###Find character###
		* f - to character
		* t - until character
###Basic Navigation###
		* gg - top of the page
		* G - bottom of the page
		
