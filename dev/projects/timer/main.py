from tkinter import *
import time
import beepy


TIME_MEASUREMENT = 60
def enter_hours():
    for h in range(0, 13):
        return h
HOURS = enter_hours()

def enter_minutes():
    for m in range(0, 61):
        if m % 5 == 0:
            return m
MINUTES = enter_minutes()

def enter_seconds():
    for s in range(0, 121):
        if s % 15 == 0:
            return s
SECONDS = enter_seconds()

#HOURS = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
#MINUTES = 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60
#SECONDS = 15, 30, 45, 60, 75, 90, 105, 120
TOTAL = HOURS * TIME_MEASUREMENT ** 2 + MINUTES * TIME_MEASUREMENT + SECONDS

def print_timer():
    while True:
        while TOTAL > 0:
            m, s = divmod(TOTAL, TIME_MEASUREMENT)
            h, m = divmod(m, TIME_MEASUREMENT)
            h = str(h)
            m = str(m)
            s = str(s)
            my_label.config(text=f"{h.zfill(2)}:{m.zfill(2)}:{s.zfill(2)}")
            my_label.after(1000, print_timer)


root = Tk()
root.title('My Timer Program')
root.geometry('600x600')


clicked = IntVar()
clicked_1 = IntVar()
clicked_2 = IntVar()

drop = OptionMenu(
    root, 
    clicked, 
    command=enter_hours).grid(row=3, column=3)

drop_1 = OptionMenu(
    root, 
    clicked_1, 
    command=enter_minutes).grid(row=3, column=6)

drop_2 = OptionMenu(
    root, 
    clicked_2, 
    command=enter_seconds).grid(row=3, column=9)

my_label = Label(root, text="", font=("Helvetica",48), fg="white", bg="black")
enter_button = Button(root, text="ENTER", command=print_timer).grid(row=8, column=6)
#drop_1 = OptionMenu(root, clicked, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60)
#drop_2 = OptionMenu(root, clicked, 15, 30, 45, 60, 75, 90, 105, 120)


root.mainloop()

#.grid(row=0, column=1)
