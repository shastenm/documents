
"#""         Acoustic Guitar Resource        \
 #       =============================== \
 #       I'm a life-long acoustic guitar \
 #       fingerstyle player, and I often \
 #       come-up with the wildest tunings,\
 #       capo movements, etc. To date, I \
 #       haven't seen any resource that \
        will allow me to see scales on \
        a fretboard and also account for \
        alternate tunings and capo movements \
        and so this is an attempt at creating \
        something that I would be able to use."""

NOTES = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'] * 3
MAJOR_SCALE_INTERVOL = [0, 2, 4, 5, 7, 9, 11]

ROOT = 'E'
START = 0
n = ""

standard_tuning = [ 
       NOTES.index('E'), 
       NOTES.index('B'),   
       NOTES.index('G'), 
       NOTES.index('D'), 
       NOTES.index('A'),
       NOTES.index('E'),
       ]
    


def get_major_scale():
    """This returns major scale notes and establishes a variable 'major_scale'"""
    return [NOTES[(y+NOTES.index(ROOT))%len(NOTES)] for y in MAJOR_SCALE_INTERVOL]
major_scale = get_major_scale()

def print_notes():
    for i in standard_tuning:
        strings = NOTES[i:i+15]
        
        for n in strings:
            if n in major_scale:        
                
                print("        ", n, "", end="")
            
            else:
                print("        ", '   ', "", end ="")
        print('')

print_notes()

print('_____' * 37)

for i in range(START, START+15):
    if i % 2 == 1:
        print('           ', i, end="")

    else:
        print('           ', end='')

#def print_strints():
    #say there are 4 strings
    #ret
#var = print_strings()
# 
#if strings == '5':
#    return var + string  

