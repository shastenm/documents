import math, os, random, re, sys 

def diagonal_difference(arr): 
    l_diagonal = []
    r_diagonal = []
    center = []
    """for i in range(0, len(arr)):
        l_diagonal = arr[0] [0] + arr[1][1] - arr[2][2]
        r_diagonal = arr[2] [0] + arr[1][1] - arr[0][2]
        center = arr[1][0] + arr[1][1] + arr[1][2]"""

if __name__ == "__main__":
    ftpr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    arr = []

    for _ in range(n):
        arr.append(list(map(int, input().rstrip().split())))

    result = diagonal_difference(arr)

    ftpr.write(str(result) + '\n')

    ftpr.close()


