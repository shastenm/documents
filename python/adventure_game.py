import sys
import random as r



def enter_yellow_room():
    choose_dragon_type()

def enter_blue_room():
    choose_dragon_type()

def enter_green_room():
    choose_dragon_type()

def start_game():
    decision = """  Which room would you like to enter? 
        [1] - Red
        [2] - Yellow
        [3] - Blue
        [4] - Green
        """
    
    print(decision)

    player_input = int(input("Which door would you like to enter? "))
    if player_input == "1":
        enter_red_room()
    elif player_input == "2":
        enter_green_room()
    elif player_input == "3":
        enter_blue_room()
    elif player_input == "4":
        enter_green_room()
    
def enter_red_room():
    choose_dragon_type()

def make_random_counter():
    return r.randint(0,5)
ran_counter = make_random_counter()


def choose_dragon_type():
    dragons = ['RED', 'YELLOW', 'BLUE', 'GREEN', 'none']
    if ran_counter == '0':
        return f"There's a {dragons[0]}."
    elif ran_counter == '1':
        return f"There's a {dragons[1]}."

def main():
    greeting = """
        Welcome to my Awesome Action Game.
        
        [1] - Start Game
        [2] - Exit
        
        Have Fun!!"""

    print(greeting)
    
    player_input = int(input("Choose an option: "))

    if player_input == "1":
        start_game()
    elif player_input == "2":
        print("Good-bye")
        sys.exit()

if __name__ == '__main__':
    main()  
    start_game() 
    make_random_counter()
    choose_dragon_type()






