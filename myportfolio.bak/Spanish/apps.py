from django.apps import AppConfig


class SpanishConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "Spanish"
