from django.apps import AppConfig


class ChordfinderConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ChordFinder"
