from django.urls import path
from . import views

app_name='app12'
urlpatterns = [
    path('',views.index, name='app12')
]
