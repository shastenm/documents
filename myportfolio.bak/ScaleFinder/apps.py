from django.apps import AppConfig


class ScalefinderConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ScaleFinder"
