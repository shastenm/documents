"""Portfolio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("admin/", admin.site.urls),
    path("main/", include('main.urls', namespace='app1')),
    path("English/", include('English.urls', namespace='app2')),
    path("Spanish/", include('Spanish.urls', namespace='app3')),
    path("Articles/", include('Articles.urls', namespace='app5')),
    path("Articulos/", include('Articulos.urls', namespace='app6')),
    path("Resume/", include('Resume.urls', namespace='app4')),
    path("Projects/", include('Projects.urls', namespace='app7')),
    path("Meditation/", include('Meditation.urls', namespace='app8')),
    path("MoneyTracker/", include('MoneyTracker.urls', namespace='app9')),
    path("BugTracker/", include('BugTracker.urls', namespace='app10')),
    path("ScaleFinder/", include('ScaleFinder.urls', namespace='app11')),
    path("ChordFinder/", include('ChordFinder.urls', namespace='app12')),
]
