from django.urls import path
from . import views

app_name='app5'
urlpatterns = [
    path('',views.index, name='app5')
]
