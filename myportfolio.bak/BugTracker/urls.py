from django.urls import path
from . import views

app_name='app10'
urlpatterns = [
    path('',views.index, name='app10')
]
