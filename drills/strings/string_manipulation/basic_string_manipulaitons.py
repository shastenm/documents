"""This is basically the all the list methods in Python. I will have a cheatsheet and a YT Video with this showing you which method should be used with each challenge. Try NOT to use these if possible because you will pick it up faster and remember it longer. I just went down the list from https://www.w3schools.com/python/python_ref_string.asp That is where you should start"""

# NOTE: YOU CAN TEST YOUR STRINGS USING A SIMPLE print() FUNCTION OF YOUR VARIABLES. WHAT I MEAN WHEN I SAY CREATE ANOTHER STRING IS CREATE ANOTHER VARIABLE AND GIVE IT A DIFFERENT STRING.

##############################################################################################################

# create a variable and give it a string

# Create a numeric string

# create another string

# Capitalize the first char to uppercase (two methods)

# Convert string to lower case (two methods work for this. Try them out.)

# Create another string (preferably one that spans several sentences) and center it.

# Count the number of occurences that something happens in a string. Try it with both letters and words.

# Create another string that is in a foreign language that uses the latin alphabet, and encode the string to have a German umelaut or a Spanish accentuation mark.

# Choose a string that is one word or number long, and add your favorite suffix to it.

# Go back to string that was several sentences long and set the tab size

# find the first instance of a specified value of a string

# create another long string and format it using the different formatting types

# make an f-string out of that same long string

# make another string and format specified values in a string

# search for a specified value of a string

# create another string and use the different methods that return a boolean value

# Convert the elementsof an iterable into a string

# return a left justified version of a string

# return a left trim version of a string

# use the maketrans() method in a string. HINT: You have to use more than one variable for this to work correctly.

# return a tuple of three parts of a string

# return a string where a specif value is replaced with a specific value

# searches the last position of a specific value

# search the string for a value and return last position where it was found (two different methods).

# return a right justified version of the string

# split the string at the separator

# split the string at the line breaks

# use a method that returns a true boolean value if the string starts with the specific value

# return a trimmed version of the string from both sides

#swap cases, lower for upper and vice versa

# return a translated string

#convert to uppercase

# fill the string with a specified number of 0s at the beginning of the string


















#Questions
# what are the benefits to using the format() method vs the f-string?
# what are the differences between find and index?
# what is the datatype after the split method is used?



