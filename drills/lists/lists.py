
"""Here are some exercises for list methods. I will have a cheetsheet and a youtube video out soon, but try to NOT consult those resources because I think that it will go a long way to hard-wiring these methods in your memory to use when you need to. I got these methods straight off of https://www.w3schools.com/python/python_ref_functions.asp"""

##############################################################################################################################################################################################################################

# make a list and link it to a variable and print out the result
# print out different indexes of that list
# access items in list
# change list items
# add list items
# remove list items
# loop lists
# join lists
# add an element to the end of the list
# remove all elements from the list
# return a copy of the list
# return the number of elements with the specified value
# add the elements of a list or any iterable to the end of the current list
# return the index of the first element with the specified value
# add an element at the specified position
# removes the element at the specified position
# removes the first item with the specified value
# reverses the order of the list
# sorts the list

