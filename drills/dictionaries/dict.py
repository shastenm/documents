
"""Here are some exercises for built-in dictionary methods. I will have a cheetsheet and a youtube video out soon, but try to NOT consult those resources because I think that it will go a long way to hard-wiring these methods in your memory to use when you need to. I got these methods straight off of https://www.w3schools.com/python/python_ref_functions.asp"""

##############################################################################################################################################################################################################################

# create a dictionary or pull one from the internet
# access items of the dictionary
# change items of a dictionary
# remove items of a dictionary
# loop through dictionaries
# make a nested dictionary
# access the different keys and values of that dictionary
# remove all the elements from the dictionary
# return a copy of the dictionary
# return a dictionary with specified keys and value
# return the value of the specified key
# returns a list containing a tuple for each key value pair
# returns a list containing the dictionary's keys
# removes a element with the specified key
# remove the last inserted key-value pair
# return the value of the specified key
# updates dictionary with the specified key-value pairs
# returns a list of all the values in the dictionary

