<h1>Functional Programming</h1>

<p>The idea of this section is to checkout different <em>libraries</em> in <b>Functional Programming <em>paradigms.</em></b> They are straight out of the book <b>Functional Programming Python</b>.</p>

<p>The libraries are:
	<ol>
		<li><b>itertools</b></li>
		<li><b>multipledispatch</b></li>
		<li><b>pyrsistent</b></li>
		<li><b>tools</b></li>
		<li><b>hypothesis</b></li>
		<li><b>more_itertools</b></li>
	</ol>
</p>




