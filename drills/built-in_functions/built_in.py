"""Here are some exercises for built-in functions. I will have a cheetsheet and a youtube video out soon, but try to NOT consult those resources because I think that it will go a long way to hard-wiring these methods in your memory to use when you need to. I got these methods straight off of https://www.w3schools.com/python/python_ref_functions.asp"""

##############################################################################################################################################################################################################################

# use the method with the absolute value of a number
# use the method to express the absolute value of a complex number
# use the method to checks that all the items in an iterable are true or false (make it return true and false)
# use the method that checks if ANY items in an iterable are true.
# replace non-ascii characters with escape characters
# return a binary version of a number
# return a boolean value of a specified object
# return an array of bytes
# return a bytes object
# method that returns True if the object is callable
# return a character from the specified Unicode code.
# convert method into a class method
# return a specified source as an object, ready to be executed
# delete a specified attribute(property or method)
# return a dictionary(array)
# return a list of the specified object's properties and methods
# return the quotient and the remainder when argument1 is divided by argument2
# return an enumerate object from a collection
# evaluate and execute an expression
# execute the specified code or object
# return a frozenset object
# return value of specified attribute(property or method)
# return a hash value of an object
# execute the built-in help system
# convert number into a hexadecimal value
# return the id of an object
# returns true if a specified object is an instance of a specified object
# returns true if a specified class is a subclass of a specified object
# return an iterator object
# returns a list
# return the length of an object
# return an updated dictionary of the current local symbol table
# return the largest item in an iterable
# return a memory view object
# returns the smallest item inan iterable
# return the next item in an iterable
# return a new object
# convert number into an octal
# convert an integer representing the Unicode of the specified character
# return the value of x to the power of y
# get set and delete a property
# return a readable version of an object
# return a reversed iterator
# round a number
# return a new set object
# set an attribute (property/method) of an object
# return a sliced object
# return a sorted list
# converts method into a static method
# sum items of an iterator
# return the __dict__ property of an object
# return an iterator, from two or more iterators

#QUESTIONS
#What boolean value does 0 return?

