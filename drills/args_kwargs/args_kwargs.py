
"""Here are some exercises for built-in *arg functions. I will have a cheetsheet and a youtube video out soon, but try to NOT consult those resources because I think that it will go a long way to hard-wiring these methods in your memory to use when you need to. I got these methods straight off of https://www.w3schools.com/python/python_ref_functions.asp"""

##############################################################################################################################################################################################################################

# create an *arg variable in a function and call it.
# create a *kwargs variable in a function and call it.

def my_func(**kid):

    name = ""

    def name_used(fname, lname):

        if kid["fname"]:
            name = "first"
            return fname

        else:
            name = "last"
            return lname
    name = name_used(fname, lname)
    return f"His {name} name is " + kid["lname"]

print(my_func(fname = "Tobias", lname="Refsnes"))

