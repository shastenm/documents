
"""Here are some exercises for built-in tuple methods. I will have a cheetsheet and a youtube video out soon, but try to NOT consult those resources because I think that it will go a long way to hard-wiring these methods in your memory to use when you need to. I got these methods straight off of https://www.w3schools.com/python/python_ref_functions.asp"""

##############################################################################################################################################################################################################################

# create a tuple and link it to a variable
# access a tuple
# update a tuple
# unpack a tuple
# loop a tuple
# join tuples
# return the number of times a specified value occurs in a tuple
# search the tuple for a specified value and returns the position of where it was
