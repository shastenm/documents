#variable = 'parangaricutirimicuaro'
#print(variable.find('gar'))

#txt = "For only ${price: .2f} dollars!"
#print(txt.format(price = 49))

#txt = "The binary version of {0} is {0:b}"
#print(txt.format(5))

#txt = "The temperature is between {:-} and {:-} degrees celsius."
#print(txt.format(-30, 40))

#txt = "you scored {:.0%}"
#print(txt.format(.65))

from pathlib import Path
import glob

p = Path('.')

files = list(p.glob('/home/tuxer/Documents/dev/**.*py'))
print(files)
