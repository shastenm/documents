
"""Here are some exercises for built-in sets and set methods. I will have a cheetsheet and a youtube video out soon, but try to NOT consult those resources because I think that it will go a long way to hard-wiring these methods in your memory to use when you need to. I got these methods straight off of https://www.w3schools.com/python/python_ref_functions.asp"""

##############################################################################################################################################################################################################################

# create a set and link it to a variable
# access set items
# add set items
# remove set items
# loop through sets
# join sets
# remove all the elements from the set
# returns a copy of the set
# return a set containing the difference between two or more sets
# removes the items in this set that are also included in another specified set
# remove the specified item
# returns a set that is the intersections of two other sets
# removes the items in this set that are not present in other specified sets
# returns whether two sets have a intersection or not
# returns whether another set contains this set or not
# returns whether this set contains another set or not
# remove an element from the set
# remove the specified element
# returns a set with the symmetric differences of two sets
# inserts the symmetric differences from this set and another
# return a set containing the union of sets
# update the set with a union of this set and others

